import datetime

from django.db import models


class Product(models.Model):
    name = models.CharField(max_length=100)
    category = models.CharField(max_length=100)
    active = models.BooleanField(default=False)
    price = models.FloatField()

    def orders_last_month(self):
        today = datetime.date.today()
        first_day = today.replace(day=1)
        last_month_first_day = (first_day - datetime.timedelta(days=1)).replace(day=1)
        return Order.objects.filter(product=self, created_at__range=(last_month_first_day, first_day)).count()

    def orders_current_month(self):
        today = datetime.date.today()
        first_day = today.replace(day=1)
        return Order.objects.filter(product=self, created_at__gte=first_day).count()

    def __str__(self):
        return self.name


class Order(models.Model):
    product = models.ForeignKey(Product, on_delete=models.CASCADE)
    created_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return f"Order - {self.product} - {self.created_at}"
