# Skyeng заказы в django


### Настройка окружения

```bash
poetry install
python3 skyeng/manage.py makemigrations
python3 skyeng/manage.py migrate
git config core.hooksPath .githooks
```

### Локальный запуск бекенда в докере
```bash
docker-compose up --build
```

### Локальный запуск бекенда
```bash
python3 skyeng/manage.py runserver
```

### Чтобы загрузить тестовые данные
```bash
bash load-sample-data.sh
```

#####
by Вереин Вадим