FROM python:3.10

WORKDIR /app
ENV POETRY_VIRTUALENVS_CREATE=0 \
    PYTHONPATH=/app/

ENV PATH /root/.local/bin:$PATH

COPY pyproject.toml poetry.lock ./
RUN curl -sSL https://install.python-poetry.org | python3 - && poetry config virtualenvs.create false

RUN poetry install

COPY ./skyeng .
